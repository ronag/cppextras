/*
* Copyright (c) 2012 Robert Nagy
*
* This file is part of CPPExtras.
*
* CPPExtras is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* CPPExtras is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with CPPExtras. If not, see <http://www.gnu.org/licenses/>.
*
* Author: Robert Nagy (ronag89@gmail.com)
*/
#pragma once

namespace cppex { namespace detail {

template<typename T>
class scoped
{
public:         
	explicit scoped(T&& exit) : exit_(std::forward<T>(exit)){}
	~scoped(){try{exit_();}catch(...){}}
private:
	T exit_;
};          

template <typename T>
scoped<T> create_scoped(T&& exit)
{
	return scoped<T>(std::forward<T>(exit));
}
	
}}

#define _SCOPED_LINENAME_CAT(name, line) name##line
#define _SCOPED_LINENAME(name, line) _SCOPED_LINENAME_CAT(name, line)
#define SCOPED(f) const auto& _SCOPED_LINENAME(SCOPED, __LINE__) = ::detail::create_scoped(f);_SCOPED_LINENAME(SCOPED, __LINE__);
