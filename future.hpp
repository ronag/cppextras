/*
* Copyright (c) 2012 Robert Nagy
*
* This file is part of CPPExtras.
*
* CPPExtras is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* CPPExtras is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with CPPExtras. If not, see <http://www.gnu.org/licenses/>.
*
* Author: Robert Nagy (ronag89@gmail.com)
*/
#pragma once

#include "enum_class.h"

#include "except.h"

#include <boost/thread/future.hpp>
#include <boost/thread/thread.hpp>
#include <boost/shared_ptr.hpp>

#include <functional>

namespace cppex { namespace detail {
	
template<typename R>
struct future_object_helper
{	
	template<typename T, typename F>
	static void nonlocking_invoke(T& future_object, F& f)
	{				
        try
        {
			future_object.mark_finished_with_result_internal(f());
        }
        catch(...)
        {
			future_object.mark_exceptional_finish_internal(boost::current_exception());
        }
	}

	template<typename T, typename F>
	static void locking_invoke(T& future_object, F& f)
	{				
        try
        {
			future_object.mark_finished_with_result(f());
        }
        catch(...)
        {
			future_object.mark_exceptional_finish();
        }
	}
};

template<>
struct future_object_helper<void>
{	
	template<typename T, typename F>
	static void nonlocking_invoke(T& future_object, F& f)
	{				
        try
        {
			f();
			future_object.mark_finished_with_result_internal();
        }
        catch(...)
        {
			future_object.mark_exceptional_finish_internal(boost::current_exception());
        }
	}

	template<typename T, typename F>
	static void locking_invoke(T& future_object, F& f)
	{				
        try
        {
			f();
			future_object.mark_finished_with_result();
        }
        catch(...)
        {
			future_object.mark_exceptional_finish();
        }
	}
};

template<typename R, typename F>
class deferred_future_object : public boost::detail::future_object<R>
{	
	deferred_future_object(const deferred_future_object&);
	deferred_future_object& operator=(const deferred_future_object&);

	F f;
public:

	template<typename F2>
	deferred_future_object(F2&& f)
		: f(std::forward<F2>(f))
	{
		set_wait_callback(std::mem_fn(&cppex::detail::deferred_future_object<R, F>::operator()), this);
	}

	~deferred_future_object()
	{
	}
		
	void operator()()
	{		
		future_object_helper<R>::locking_invoke(*this, f);
	}
};

template<typename R, typename F>
class async_future_object : public boost::detail::future_object<R>
{	
	async_future_object(const async_future_object&);
	async_future_object& operator=(const async_future_object&);

	F f;
	boost::thread thread;
public:

	template<typename F2>
	async_future_object(F2&& f)
		: f(std::forward<F2>(f))
		, thread([this]{run();})
	{
	}

	~async_future_object()
	{
		thread.join();
	}

	void run()
	{
		win32_exception::install_handler();
		future_object_helper<R>::locking_invoke(*this, f);
	}
};

}
	
struct launch_policy_def
{
	enum type
	{
		async = 1,
		deferred = 2
	};
};
typedef enum_class<launch_policy_def> launch;

template<typename F>
auto async(launch policy, F&& f) -> boost::unique_future<decltype(f())>
{		
	typedef decltype(f())								result_type;	
	typedef boost::detail::future_object<result_type>	future_object_type;

	boost::shared_ptr<future_object_type> future_object;

	// HACK: This solution is a hack to avoid modifying boost code.

	if((policy & launch::async) != 0)
		future_object.reset(new cppex::detail::async_future_object<result_type, F>(std::forward<F>(f)), [](future_object_type* p)
			{
				delete reinterpret_cast<cppex::detail::async_future_object<result_type, F>*>(p);
			});
	else if((policy & launch::deferred) != 0)
		future_object.reset(new cppex::detail::deferred_future_object<result_type, F>(std::forward<F>(f)), [](future_object_type* p)
			{
				delete reinterpret_cast<cppex::detail::deferred_future_object<result_type, F>*>(p);
			});
	else
		throw std::invalid_argument("policy");
	
	boost::unique_future<result_type> future;

	static_assert(sizeof(future) == sizeof(future_object), "");

	reinterpret_cast<boost::shared_ptr<future_object_type>&>(future) = std::move(future_object); // Get around the "private" encapsulation.
	return std::move(future);
}
	
template<typename F>
auto async(F&& f) -> boost::unique_future<decltype(f())>
{	
	return async(launch(launch::async | launch::deferred), std::forward<F>(f));
}

template<typename T>
auto make_async(T&& value) -> boost::unique_future<T>
{	
	return async(launch::deferred, std::bind(std::function<T(const T&)>([](const T& value)
	{
		return value;
	}), std::forward<T>(value)));
}

inline boost::unique_future<void> make_async()
{	
	return async(launch::deferred, []{});
}

template<typename T>
auto make_shared(boost::unique_future<T>&& f) -> boost::shared_future<T>
{	
	return boost::shared_future<T>(std::move(f));
}


template<typename T>
struct fold_future_traits
{
	typedef T result_type;
	
	static auto get(T& value) -> T&
	{
		return value;
	}
};

template<>
struct fold_future_traits<void>
{
	typedef void result_type;
	
	static auto get() -> void
	{
	}
};

template<typename T>
struct fold_future_traits<boost::unique_future<T>>
{
	typedef typename fold_future_traits<T>::result_type result_type;

	static auto get(boost::unique_future<T>& value) -> decltype(boost::declval<boost::unique_future<T>>().get())
	{
		return value.get();
	}
};

template<>
struct fold_future_traits<boost::unique_future<void>>
{
	typedef void result_type;

	static auto get(boost::unique_future<void>& value) -> void
	{
		value.get();
	}
};

template<typename T>
struct fold_future_traits<boost::shared_future<T>>
{
	typedef typename fold_future_traits<T>::result_type result_type;
	
	static auto get(boost::shared_future<T>& value) -> decltype(boost::declval<boost::shared_future<T>>().get())
	{
		return value.get();
	}
};

template<>
struct fold_future_traits<boost::shared_future<void>>
{
	typedef void result_type;

	static auto get(boost::shared_future<void>& value) -> void
	{
		value.get();
	}
};

template<typename T>
class fold_future_impl : boost::noncopyable
{
	T f_;
public:	

	typedef fold_future_traits<T> traits;

	template<typename T2>
	fold_future_impl(T2&& f)
		: f_(std::forward<T2>(f))
	{
	}

	fold_future_impl(fold_future_impl&& other)
		: f_(std::move(other.f_))
	{
	}

	fold_future_impl& operator=(fold_future_impl&& other)
	{
		f_ = std::move(other.f_);
		return *this;
	}

	typename fold_future_traits<T>::result_type operator()()
	{
		return traits::get(f_);
	}
};

template<typename T>
class fold_future_impl<boost::unique_future<T>> : boost::noncopyable
{
	boost::unique_future<T> f_;
public:	

	typedef fold_future_traits<boost::unique_future<T>> traits;
	
	template<typename T2>
	fold_future_impl(T2&& f)
		: f_(std::forward<T2>(f))
	{
	}

	fold_future_impl(fold_future_impl&& other)
		: f_(std::move(other.f_))
	{
	}

	fold_future_impl& operator=(fold_future_impl&& other)
	{
		f_ = std::move(other.f_);
		return *this;
	}

	typename traits::result_type operator()()
	{
		return fold_future_impl<T>(traits::get(f_))();
	}
};

template<>
class fold_future_impl<boost::unique_future<void>> : boost::noncopyable
{
	boost::unique_future<void> f_;
public:	

	typedef fold_future_traits<boost::unique_future<void>> traits;
	
	fold_future_impl(boost::unique_future<void>&& f)
		: f_(std::move(f))
	{
	}

	fold_future_impl(fold_future_impl&& other)
		: f_(std::move(other.f_))
	{
	}

	fold_future_impl& operator=(fold_future_impl&& other)
	{
		f_ = std::move(other.f_);
		return *this;
	}

	void operator()()
	{
		f_.get();
	}
};

template<typename T>
class fold_future_impl<boost::shared_future<T>> : boost::noncopyable
{
	boost::shared_future<T> f_;
public:	

	typedef fold_future_traits<boost::shared_future<T>> traits;
	
	fold_future_impl(const boost::shared_future<T>& f)
		: f_(f)
	{
	}

	fold_future_impl(boost::shared_future<T>&& f)
		: f_(std::move(f))
	{
	}

	fold_future_impl(fold_future_impl&& other)
		: f_(std::move(other.f_))
	{
	}

	fold_future_impl& operator=(fold_future_impl&& other)
	{
		f_ = std::move(other.f_);
		return *this;
	}

	typename traits::result_type operator()()
	{
		return fold_future_impl<T>(traits::get(f_))();
	}
};

template<>
class fold_future_impl<boost::shared_future<void>> : boost::noncopyable
{
	boost::shared_future<void> f_;
public:	

	typedef fold_future_traits<boost::shared_future<void>> traits;
	
	template<typename T2>
	fold_future_impl(T2&& f)
		: f_(std::forward<T2>(f))
	{
	}

	fold_future_impl(fold_future_impl&& other)
		: f_(std::move(f_))
	{
	}

	fold_future_impl& operator=(fold_future_impl&& other)
	{
		f_ = std::move(other.f_);
		return *this;
	}

	void operator()()
	{
		f_.get();
	}
};

template<typename T>
auto fold_future(boost::unique_future<T>&& f) -> boost::unique_future<typename fold_future_traits<T>::result_type>
{
    return async(launch::deferred, fold_future_impl<boost::unique_future<T>>(std::move(f)));
}

}