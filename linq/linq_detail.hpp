#pragma once

#include <boost/cast.hpp>
#include <boost/implicit_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <boost/optional.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/utility/declval.hpp>

#include <memory>
#include <tuple>

namespace cppex { namespace linq_detail {

struct second_selector
{
	template<typename T>
	auto operator()(const T& t) const -> const decltype(t.second)&
	{
		return t.second;
	}
};

struct first_selector
{
	template<typename T>
	auto operator()(const T& t) const -> const decltype(t.first)&
	{
		return t.first;
	}
};

template<typename U>
struct polymorhpic_downcaster
{
	template<typename T>
	auto operator()(const T& t) const -> U
	{
		return boost::polymorphic_downcast<U>(t);
	}

	template<typename T>
	auto operator()(const std::shared_ptr<T>& t) const -> U
	{
		return boost::shared_polymorphic_downcast<U>(t);
	}

	template<typename T>
	auto operator()(const boost::shared_ptr<T>& t) const -> U
	{
		return boost::shared_polymorphic_downcast<U>(t);
	}
};

template<typename U>
struct polymorhpic_caster
{
	template<typename T>
	auto operator()(const T& t) const -> U
	{
		return boost::polymorphic_cast<U>(t);
	}

	template<typename T>
	auto operator()(const std::shared_ptr<T>& t) const -> U
	{
		return boost::shared_polymorphic_cast<U>(t);
	}

	template<typename T>
	auto operator()(const boost::shared_ptr<T>& t) const -> U
	{
		return boost::shared_polymorphic_cast<U>(t);
	}
};

template<typename U>
struct implicit_caster
{
	template<typename T>
	auto operator()(const T& t) const -> U
	{
		return boost::implicit_cast<U>(t);
	}
};

template<typename U>
struct numeric_caster
{
	template<typename T>
	auto operator()(const T& t) const -> U
	{
		return boost::numeric_cast<U>(t);
	}
};

template<typename U>
struct static_caster
{
	template<typename T>
	auto operator()(const T& t) const -> U
	{
		return static_cast<U>(t);
	}
};

template<typename F>
struct zip_operator
{
	F f;

	zip_operator(F f)
		: f(f)
	{
	}

	template<typename T>
	auto operator()(const T& t) -> decltype(boost::declval<F>()(std::get<0>(t), std::get<1>(t)))
	{
		return f(std::get<0>(t), std::get<1>(t));
	}
};

template<class T>
class value_wrapper
{    
	value_wrapper(const value_wrapper&);
	value_wrapper& operator=(const value_wrapper&);
public:

    value_wrapper() 
	{
	}

    value_wrapper(T&& value)
		: value_(std::move(value))
	{
	}
	
    value_wrapper(value_wrapper&& other)
		: value_(std::move(other.value_))
	{
	}

	value_wrapper& operator=(value_wrapper&& other)
	{
		value_ = std::move(other.value_);
		return *this;
	}

    T& operator*()
    {
		return *value_;
    }
	
    const T& operator*() const
    {
		return *value_;
    }
	
	boost::optional<T>& operator->() 
	{
		return value_:
	}
	
	const boost::optional<T>& operator->() const
	{
		return value_:
	}
	
    bool operator==(value_wrapper const& y) const
    {
		return value_ == y.value_;
    }
private:
	boost::optional<T> value_;
};

}}