/*
* Copyright (c) 2012 Robert Nagy
*
* This file is part of CPPExtras.
*
* CPPExtras is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* CPPExtras is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with CPPExtras. If not, see <http://www.gnu.org/licenses/>.
*
* Author: Robert Nagy (ronag89@gmail.com)
*/
#pragma once

#include "linq_detail.hpp"

#include <boost/algorithm/minmax_element.hpp>
#include <boost/generator_iterator.hpp>
#include <boost/iterator.hpp>
#include <boost/range.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/combine.hpp>
#include <boost/utility/declval.hpp>

#include <algorithm>
#include <iterator>
#include <numeric>
#include <set>
#include <type_traits>

namespace cppex {

template<typename I>
class first_or
{
	I it_;
	I end_;

	typedef void (first_or::*bool_type)() const;
    void this_type_does_not_support_comparisons() const {}

public:

	typedef decltype(*boost::declval<I>()) value_type;

	first_or(I it, I end)
		: it_(it), end_(end)
	{
	}

	value_type value_or(const value_type& value) const
	{
		if(it_ == end_)
			return value;

		return *it_;
	}
	
	value_type value_or(const value_type& value)
	{
		if(it_ == end_)
			return value;

		return *it_;
	}

	const value_type& operator*() const
	{
		if(!*this)
			throw logic_error();

		return *it_;
	}
	
	value_type& operator*()
	{
		if(!*this)
			throw logic_error();

		return *it_;
	}

	I operator->() const
	{
		if(!*this)
			throw logic_error();

		return it_;
	}

	I operator->()
	{
		if(!*this)
			throw logic_error();

		return it_;
	}

	const I& get() const
	{
		return it_;
	}

	operator bool_type() const 
	{
		return it_ != end_ ? &first_or::this_type_does_not_support_comparisons : 0;
    }
};

template<typename I>
first_or<I> make_first_or(I begin, I end)
{
	return first_or<I>(begin, end);
}

template<typename R>
class linq_range
{
	linq_range(const linq_range&);
	linq_range& operator=(const linq_range&);

	R range_;
	
	typedef typename R::iterator	iterator;	
	typedef typename R::value_type	value_type;	

	template<typename F>
	static auto select_type_helper(const F& f) ->  decltype(boost::declval<R>() | boost::adaptors::transformed(f))
	{
		return boost::declval<R>() | boost::adaptors::transformed(f);
	}

public:

	template<typename T>
	linq_range(T&& range_)
		: range_(std::forward<T>(range_))
	{
	}

	linq_range(linq_range&& other)
		: range_(std::move(other.range_))
	{
	}
	
	linq_range& operator=(linq_range&& other)
	{
		range_ = std::move(range_);
	}

	auto first() const -> first_or<iterator>
	{
		return make_first_or(begin(), end());
	}
			
	template<typename F>
	auto first(const F& f) const -> first_or<iterator>
	{
		return make_first_or(std::find_if(begin(), end(), f), end());
	}
	
	auto last() const -> first_or<iterator>
	{
		if(empty())
			return first_or<iterator>(end(), end());

		return make_first_or(--end(), end());
	}
			
	template<typename F>
	auto last(const F& f) const -> first_or<iterator>
	{
		return first(reverse(), f);
	}

	template<typename F>
	auto any(const F& f) const -> bool
	{
		return std::any_of(begin(), end(), f);
	}

	template<typename F>
	auto all(const F& f) const -> bool
	{
		return std::all_of(begin(), end(), f);
	}
	
	template<typename T>
	auto contains(const T& value) const -> bool
	{
		return first(value);
	}

	template<typename F>
	auto where(const F& f) const -> linq_range
	{
		return range_ | boost::adaptors::filtered(f);
	}
	
	template<typename F>
	void for_each(const F& f) const
	{
		std::for_each(begin(), end(), f);
	}

	template<typename F>
	auto select(const F& f) const -> linq_range<decltype(boost::declval<R>() | boost::adaptors::transformed(f))>
	{
		return range_ | boost::adaptors::transformed(f);
	}
	
	template<typename R2, typename F>
	auto zip(const R2& other, const F& f) const -> linq_range<decltype(boost::combine(boost::declval<R>(), boost::declval<R2>()) | boost::adaptors::transformed(linq_detail::zip_operator<F>(f)))>
	{
		return boost::combine(range_, other) | boost::adaptors::transformed(linq_detail::zip_operator<F>(f));
	}

	auto reverse() const -> linq_range<decltype(boost::declval<R>() | boost::adaptors::reversed)>
	{
		return range_ | boost::adaptors::reversed;
	}
	
	template<typename T>
	auto take(T n) const -> linq_range<decltype(boost::declval<R>() | boost::adaptors::sliced(0, n))>
	{
		return range_ | boost::adaptors::sliced(0, n);
	}
		
	template<typename F>
	auto take_while(const F& f) const -> linq_range<decltype(boost::make_iterator_range(boost::end(boost::declval<R>()), boost::end(boost::declval<R>())))>
	{
		return boost::make_iterator_range(begin(), first([=](const value_type& x){return !f(x);}).get());
	}

	template<typename T>
	auto skip(T n) const -> linq_range<decltype(boost::make_iterator_range(boost::end(boost::declval<R>()), boost::end(boost::declval<R>())))>
	{
		return boost::make_iterator_range(element_at(n).get(), end());
	}

	template<typename F>
	auto skip_while(const F& f) const -> linq_range<decltype(boost::make_iterator_range(boost::end(boost::declval<R>()), boost::end(boost::declval<R>())))>
	{
		return boost::make_iterator_range(first([=](const value_type& x){return !f(x);}).get(), end());
	}

	auto keys() const -> linq_range<decltype(select_type_helper(linq_detail::first_selector()))>
	{
		return select(linq_detail::first_selector());
	}
		
	auto values() const -> linq_range<decltype(select_type_helper(linq_detail::second_selector()))>
	{
		return select(linq_detail::second_selector());
	}
	
	template<typename F>
	auto min(const F& f) const -> first_or<iterator>
	{
		return make_first_or(std::min_element(begin(), end(), f), end());
	}

	auto min() const -> first_or<iterator>
	{
		return make_first_or(std::min_element(begin(), end()), end());
	}
	
	template<typename F>
	auto max(const F& f) const -> first_or<iterator>
	{
		return make_first_or(std::max_element(begin(), end(), f), end());
	}

	auto max() const -> first_or<iterator>
	{
		return make_first_or(std::max_element(begin(), end()), end());
	}
	
	template<typename F>
	auto minmax(const F& f) const -> std::pair<first_or<iterator>, first_or<iterator>>
	{
		auto it = boost::minmax_element(begin(), end());
		return std::make_pair(make_first_or(it.first, end()), make_first_or(it.second, end()));
	}
	
	auto minmax() const -> std::pair<first_or<iterator>, first_or<iterator>>
	{
		auto it = boost::minmax_element(begin(), end());
		return std::make_pair(make_first_or(it.first, end()), make_first_or(it.second, end()));
	}

	auto count() const -> decltype(std::distance(boost::begin(boost::declval<R>()), boost::end(boost::declval<R>())))
	{
		return std::distance(begin(), end());
	}
	
	template<typename F>
	auto count(const F& f) const -> decltype(std::count_if(boost::begin(boost::declval<R>()), boost::end(boost::declval<R>()), f))
	{		
		return std::count_if(begin(), end(), f);
	}
	
	template<typename T>
	auto element_at(T index) const -> first_or<iterator>
	{			
		return make_first_or(std::next(begin(), index), end());
	}
		
	template<typename F>
	auto reduce(const F& f) const -> first_or<linq_detail::value_wrapper<value_type>>
	{
		if(empty())
			return make_first_or(linq_detail::value_wrapper<value_type>(), linq_detail::value_wrapper<value_type>());

		auto it    = begin();
		auto value = *it++;
		value	   = std::accumulate(it, end(), std::move(value), f);

		return make_first_or(linq_detail::value_wrapper<value_type>(std::move(value)), linq_detail::value_wrapper<value_type>());
	}
	
	auto sum() const -> first_or<linq_detail::value_wrapper<value_type>>
	{
		return reduce(std::plus<value_type>());
	}

	auto average() const -> first_or<linq_detail::value_wrapper<value_type>>
	{
		if(empty())
			return make_first_or(linq_detail::value_wrapper<value_type>(), linq_detail::value_wrapper<value_type>());

		return reduce(std::plus<value_type>())/count();
	}

	template<typename T>
	auto cast() const -> typename std::enable_if<std::is_arithmetic<T>::value, linq_range<decltype(select_type_helper(linq_detail::static_caster<T>()))>>::type
	{
		return select(linq_detail::static_caster<T>());
	}
	
	template<typename T>
	auto implicit_cast() const -> linq_range<decltype(select_type_helper(linq_detail::implicit_caster<T>()))>
	{
		return select(linq_detail::implicit_caster<T>());
	}
	
	template<typename T>
	auto polymorphic_downcast() const -> linq_range<decltype(select_type_helper(linq_detail::polymorhpic_downcaster<T>()))>
	{
		return select(linq_detail::polymorhpic_downcaster<T>());
	}
	
	template<typename T>
	auto polymorphic_cast() const -> linq_range<decltype(select_type_helper(linq_detail::polymorhpic_caster<T>()))>
	{
		return select(linq_detail::polymorhpic_caster<T>());
	}

	template<typename T>
	auto numeric_cast() const -> linq_range<decltype(select_type_helper(linq_detail::numeric_caster<T>()))>
	{
		return select(linq_detail::numeric_caster<T>());
	}
	
	template<typename C> 
	auto to() const -> C
	{
		return C(begin(), end());
	}

	auto distinct() const -> std::set<value_type>
	{
		return std::set<value_type>(begin(), end());
	}
	
	template<typename F>
	auto distinct(const F& f) const -> std::set<value_type, F>
	{
		return std::set<value_type, F>(begin(), end(), f);
	}

	template<typename R2>
	auto except(const linq_range<R2>& other) const -> std::set<value_type>
	{
		std::set<value_type> result;
		std::set_difference(distinct(), other.distinct(), std::inserter(result, result.end()));
		return result;
	}
	
	template<typename R2, typename F>
	auto except(const linq_range<R2>& other, const F& f) const -> std::set<value_type, F>
	{
		std::set<value_type, F> result(f);
		std::set_difference(distinct(f), other.distinct(f), std::inserter(result, result.end()));
		return result;
	}

	template<typename R2>
	auto union_(const linq_range<R2>& other) const -> std::set<value_type>
	{
		std::set<value_type> result;
		std::set_union(distinct(f), other.distinct(f), std::inserter(result, result.end()));
		return result;
	}
	
	template<typename R2, typename F>
	auto union_(const linq_range<R2>& other, const F& f) const -> std::set<value_type, F>
	{
		std::set<value_type, F> result(f);
		std::set_union(distinct(f), other.distinct(f), std::inserter(result, result.end()));
		return result;
	}
	
	template<typename R2>
	auto intersect(const linq_range<R2>& other) const -> std::set<value_type>
	{
		std::set<value_type> result;
		std::set_intersect(distinct(f), other.distinct(f), std::inserter(result, result.end()));
		return result;
	}
	
	template<typename R2, typename F>
	auto intersect(const linq_range<R2>& other, const F& f) const -> std::set<value_type, F>
	{
		std::set<value_type, F> result(f);
		std::set_intersect(distinct(f), other.distinct(f), std::inserter(result, result.end()));
		return result;
	}

	auto begin() const -> decltype(boost::begin(boost::declval<const R>()))
	{
		return boost::begin(range_);
	}

	auto end() const -> decltype(boost::begin(boost::declval<const R>()))
	{
		return boost::end(range_);
	}

	bool empty() const
	{
		return begin() == end();
	}
	
	template<typename R2>
	bool equals(const linq_range<R2>& other) const
	{
		return count() == other.count() && std::equal(begin(), end(), other.begin());
	}
	
	template<typename R2, typename F>
	bool equals(const linq_range<R2>& other, const F& f) const
	{
		return count() == other.count() && std::equal(begin(), end(), other.begin(), f);
	}

	template<typename R2>
	bool operator==(const linq_range<R2>& other) const
	{
		return equals(other);
	}

	template<typename R2>
	bool operator!=(const linq_range<R2>& other) const
	{
		return !(*this == other);
	}
};

template<typename R>
auto from(const R& r) -> linq_range<boost::iterator_range<decltype(boost::begin(r))>>
{
	return from(boost::begin(r), boost::end(r));
}

template<typename I>
linq_range<boost::iterator_range<I>> from(I b, I e)
{
	return linq_range<boost::iterator_range<I>>(boost::iterator_range<I>(b, e));
}


}
